import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationPage } from './registration';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    RegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationPage),
    ComponentsModule
  ],
  exports: [
    RegistrationPage
  ]
})
export class RegistrationPageModule {}
