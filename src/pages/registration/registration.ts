import {Component, OnInit} from '@angular/core';
import {IonicPage, ActionSheetController, NavController,Platform} from 'ionic-angular';
import {FormGroup, Validators, FormBuilder, FormArray} from "@angular/forms";
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import {L2nHttp} from "../../providers/l2n-http";
import {LocationAccuracy} from "@ionic-native/location-accuracy";

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage implements OnInit{

  public registrationForm: FormGroup;
  private base64Image="assets/cameraIcon .png";

  constructor(private formBuilder: FormBuilder,
              public navCtrl: NavController,
              private addPhotoActionSheet: ActionSheetController,
              private camera: Camera,
              private geolocation: Geolocation,
              private l2nHttp:L2nHttp,
              private locationAccuracy: LocationAccuracy,
              public platform: Platform) {

  }

  ngOnInit(){

      let options= {
        timeout: 6000,
        enableHighAccuracy: true
      };

      this.registrationForm = this.formBuilder.group({
        fullName: ['', [Validators.required]],
        nickName: [''],
        mobileOrPhone: this.formBuilder.array([
          this.initMobileOrPhone()
        ]),
        entryDateTime:[new Date().toLocaleString()],
        email: ['',Validators.compose([Validators.pattern('([a-zA-Z0-9_.]{1,})((@[a-zA-Z]{2,})[\\\.]([a-zA-Z]{2}|[a-zA-Z]{3}))')])],
        address: [''],
        area: [''],
        city: [''],
        country: [''],
        aboutThisPerson: [''],
        discussion: [''],
        record: [''],
        photo: [''],
        latitude:[''],
        longitude:['']
      });

    if(this.platform.is('android')){
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if(canRequest) {

          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)

            .then(() =>{
                this.geolocation.getCurrentPosition(options).then((resp) => {
                  this.registrationForm.controls['latitude'].setValue(resp.coords.latitude);
                  this.registrationForm.controls['longitude'].setValue(resp.coords.longitude);
                }).catch((error) => {
                  console.log('Error getting location', error);
                })

              },
              error => console.log('Error requesting location permissions', error)
            );
        }

      });
    }

  }


  initMobileOrPhone(){
    return this.formBuilder.group({
      mobileNumber:['',Validators.compose([Validators.pattern('01[7|6|5|1|9|8]{1}[0-9]{8}'), Validators.required])]
    });
  }

  submitValue(){

    this.l2nHttp.post('visitor-save',this.registrationForm.value, 'Saving data').then(
      data => {
        console.log('success');
        console.log(data);
        this.navCtrl.popToRoot();
      }, err => {
        console.log(err);
      }
    );

  }

  addMobile() {
    const control = <FormArray>this.registrationForm.controls['mobileOrPhone'];
    control.push(this.initMobileOrPhone());
  }

  removeMobile(i: number) {
    const control = <FormArray>this.registrationForm.controls['mobileOrPhone'];
    control.removeAt(i);
  }

  addMedia(){

    const actionSheet = this.addPhotoActionSheet.create({
      title: 'Add Images',
      cssClass:'custom-font',
      buttons: [
        {
          text: 'Take Photo',
          icon:'ios-camera-outline',
          handler: () => {
            this.openCamera();
          }
        },

        {
          text: 'Photo From Gallery',
          icon:'ios-albums-outline',
          handler: () => {
            this.selectFromGallery();
          }
        }
      ]
    });

    actionSheet.present();
  }

  selectFromGallery() {

    var options = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType:this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.registrationForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });
  }

  openCamera() {

    var options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.registrationForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });

  }

}
