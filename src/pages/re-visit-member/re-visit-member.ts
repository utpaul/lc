import {Component, OnInit} from '@angular/core';
import {IonicPage, NavParams, NavController, ActionSheetController, Platform} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { Geolocation } from '@ionic-native/geolocation';
import {L2nHttp} from "../../providers/l2n-http";
import {API_CONFIG} from "../../constants/api";
import { Camera } from '@ionic-native/camera';
import {LocationAccuracy} from "@ionic-native/location-accuracy";

@IonicPage()
@Component({
  selector: 'page-re-visit-member',
  templateUrl: 're-visit-member.html',
})
export class ReVisitMemberPage implements OnInit{

  private loadedList :any;
  public reRegistrationForm: FormGroup;
  private imageUrl:any;
  private base64Image="assets/cameraIcon .png";

  constructor(public navParams: NavParams,
              private geolocation: Geolocation,
              private formBuilder: FormBuilder,
              private l2nHttp:L2nHttp,
              private navCtrl: NavController,
              private addPhotoActionSheet: ActionSheetController,
              private camera: Camera,
              private locationAccuracy: LocationAccuracy,
              public platform: Platform) {
    this.platform.ready().then(() => {
      this.imageUrl =API_CONFIG.misBase;
    });

  }

  ngOnInit(){

      this.loadedList =this.navParams.data;
      this.reRegistrationForm = this.formBuilder.group({
        id:[''],
        entryDateTime:[''],
        discussion: ['',[Validators.required]],
        record: [''],
        photo: [''],
        latitude:[''],
        longitude:['']
      });

      let options= {
        timeout: 3000,
        enableHighAccuracy: true
      };

    if(this.platform.is('android')){
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if(canRequest) {

          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)

            .then(() =>{

                this.geolocation.getCurrentPosition(options).then((resp) => {
                  this.reRegistrationForm.controls['latitude'].setValue(resp.coords.latitude);
                  this.reRegistrationForm.controls['longitude'].setValue(resp.coords.longitude);
                }).catch((error) => {
                  console.log('Error getting location', error);
                })

              },
              error => console.log('Error requesting location permissions', error)
            );
        }

      });
    }

    this.reRegistrationForm.controls['entryDateTime'].setValue(new Date().toLocaleString());
    this.reRegistrationForm.controls['id'].setValue(this.loadedList.id);


  }

  submitValue(){

    this.l2nHttp.post('re-visit-member',this.reRegistrationForm.value, 'Saving data').then(
      data => {
        console.log(data);
        console.log("success in submit");
        this.navCtrl.popToRoot();
      }, err => {
        console.log(err);
      }
    );

  }

  addMedia(){

    const actionSheet = this.addPhotoActionSheet.create({
      title: 'Add Images',
      cssClass:'custom-font',
      buttons: [
        {
          text: 'Take Photo',
          icon:'ios-camera-outline',
          handler: () => {
            this.openCamera();
          }
        },

        {
          text: 'Photo From Gallery',
          icon:'ios-albums-outline',
          handler: () => {
            this.selectFromGallery();
          }
        }
      ]
    });

    actionSheet.present();
  }

  selectFromGallery() {

    var options = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType:this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.reRegistrationForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });
  }

  openCamera() {

    var options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.reRegistrationForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });

  }

}
