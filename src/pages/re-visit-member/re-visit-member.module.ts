import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReVisitMemberPage } from './re-visit-member';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    ReVisitMemberPage,
  ],
  imports: [
    IonicPageModule.forChild(ReVisitMemberPage),
    ComponentsModule
  ],
  exports: [
    ReVisitMemberPage
  ]
})
export class ReVisitMemberPageModule {}
