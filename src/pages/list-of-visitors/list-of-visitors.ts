import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";
import {AuthProvider} from "../../providers/auth";

@IonicPage()
@Component({
  selector: 'page-list-of-visitors',
  templateUrl: 'list-of-visitors.html',
})
export class ListOfVisitorsPage {

  private imageUrl:any;
  adminFlag:boolean=false;
  private loadedList =[];
  itemExpendedHeight:number=200;
  private loadedData =[];
  private converter:any;

  editedFormPage = 'EditedFormPage';

  constructor(private l2nHttp:L2nHttp,
              private auth:AuthProvider) {
    this.imageUrl =API_CONFIG.misBase;
  }

  ionViewWillEnter() {

    if(this.auth.currentUser.department =='Top Management'){

      this.adminFlag =true;
      this.l2nHttp.getRequest('visitor-list','getting data...')
        .then(
          data => {
            this.loadedData=data;

            for(let i=0; i<this.loadedData.length;i++){

              this.loadedData[i].eachDayUser =this.loadedData[i].children.length;
              this.converter =(this.loadedData[i].entrydate).split('-');

                if(parseInt(this.converter[1]) ==1){
                  this.loadedData[i].entrydate =this.converter[2]+' Jan '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==2){
                  this.loadedData[i].entrydate =this.converter[2]+' Feb '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==3){
                  this.loadedData[i].entrydate =this.converter[2]+' Mar '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==4){
                  this.loadedData[i].entrydate =this.converter[2]+' Apr '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==5){
                  this.loadedData[i].entrydate =this.converter[2]+' May '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==6){
                  this.loadedData[i].entrydate =this.converter[2]+' Jun '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==7){
                  this.loadedData[i].entrydate =this.converter[2]+' Jul '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==8){
                  this.loadedData[i].entrydate =this.converter[2]+' Aug '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==9){
                  this.loadedData[i].entrydate =this.converter[2]+' Sep '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==10){
                  this.loadedData[i].entrydate =this.converter[2]+' Oct '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==11){
                  this.loadedData[i].entrydate =this.converter[2]+' Nov '+this.converter[0];
                }else if(parseInt(this.converter[1]) ==12){
                  this.loadedData[i].entrydate =this.converter[2]+' Dec '+this.converter[0];
                }

               for(let j=0;j<this.loadedData[i].children.length;j++){

                 if( (parseInt(this.loadedData[i].children[j].roseVisited) +
                   parseInt(this.loadedData[i].children[j].lilyVisisted) +
                   parseInt(this.loadedData[i].children[j].bitheeVisited) +
                   parseInt(this.loadedData[i].children[j].beenaVisited)) > 1){

                   this.loadedData[i].children[j].revisited = 1;
                 }else{
                   this.loadedData[i].children[j].revisited = 0;
                 }
                 this.loadedData[i].children[j].open =false;
               }

            }

            this.loadedList = this.loadedData;
            console.log(this.loadedList);
          },
          err => {}
        );

    }else{

      this.adminFlag =false;
      this.l2nHttp.getRequest('visitor-list','getting data...').then(
        data => {
          console.log(data);
          this.loadedData=data;

          for(let i=0; i<this.loadedData.length;i++){

            this.loadedData[i].eachDayUser =this.loadedData[i].children.length;
            this.converter =(this.loadedData[i].entrytime).split('-');

            if(parseInt(this.converter[1]) ==1){
              this.loadedData[i].entrytime =this.converter[0]+' Jan '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==2){
              this.loadedData[i].entrytime =this.converter[0]+' Feb '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==3){
              this.loadedData[i].entrytime =this.converter[0]+' Mar '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==4){
              this.loadedData[i].entrytime =this.converter[0]+' Apr '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==5){
              this.loadedData[i].entrytime =this.converter[0]+' May '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==6){
              this.loadedData[i].entrytime =this.converter[0]+' Jun '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==7){
              this.loadedData[i].entrytime =this.converter[0]+' Jul '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==8){
              this.loadedData[i].entrytime =this.converter[0]+' Aug '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==9){
              this.loadedData[i].entrytime =this.converter[0]+' Sep '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==10){
              this.loadedData[i].entrytime =this.converter[0]+' Oct '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==11){
              this.loadedData[i].entrytime =this.converter[0]+' Nov '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==12){
              this.loadedData[i].entrytime =this.converter[0]+' Dec '+this.converter[2];
            }

            for(let j=0;j<this.loadedData[i].children.length;j++){
              this.loadedData[i].children[j].open =false;
            }

          }

          this.loadedList = this.loadedData;
          console.log(this.loadedList);

        },
        err => {}
      );

    }

  }

  toggleSection(i) {
    this.loadedList[i].open = !this.loadedList[i].open;
  }

  toggleItemSection(i,j){
    this.loadedList[i].children[j].open = !this.loadedList[i].children[j].open;
  }
}
