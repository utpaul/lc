import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOfVisitorsPage } from './list-of-visitors';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    ListOfVisitorsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListOfVisitorsPage),
    ComponentsModule
  ],
  exports: [
    ListOfVisitorsPage
  ]
})
export class ListOfVisitorsPageModule {}
