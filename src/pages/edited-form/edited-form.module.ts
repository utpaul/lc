import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditedFormPage } from './edited-form';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    EditedFormPage,
  ],
  imports: [
    IonicPageModule.forChild(EditedFormPage),
    ComponentsModule
  ],
  exports: [
    EditedFormPage
  ]
})
export class EditedFormPageModule {}
