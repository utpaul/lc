import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ActionSheetController, Platform} from 'ionic-angular';
import {FormGroup, Validators, FormBuilder, FormArray, FormControl} from "@angular/forms";
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";
import { Camera } from '@ionic-native/camera';
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-edited-form',
  templateUrl: 'edited-form.html',
})
export class EditedFormPage implements OnInit{

  private loadedList:any;
  public editedForm: FormGroup;
  private base64Image ="";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private addPhotoActionSheet: ActionSheetController,
              private camera: Camera,
              private l2nHttp:L2nHttp,
              private locationAccuracy: LocationAccuracy,
              public platform: Platform,
              private geolocation: Geolocation) {

  }

  ngOnInit() {

    let options= {
      timeout: 6000,
      enableHighAccuracy: true
    };

    this.loadedList = this.navParams.data;

    console.log(this.loadedList);

    this.base64Image = API_CONFIG.misBase + this.loadedList.photo;

    this.editedForm = this.formBuilder.group({
      id: [this.loadedList.id],
      fullName: [this.loadedList.fullName, [Validators.required]],
      nickName: [this.loadedList.nickName == '---' ? '' : this.loadedList.nickName],
      email: [this.loadedList.email == '---' ? '' : this.loadedList.email,
        Validators.compose([Validators.pattern('([a-zA-Z0-9_.]{1,})((@[a-zA-Z]{2,})[\\\.]([a-zA-Z]{2}|[a-zA-Z]{3}))')])],
      address: [this.loadedList.address == '---' ? '' : this.loadedList.address],
      area: [this.loadedList.area == '---' ? '' : this.loadedList.area],
      city: [this.loadedList.city == '---' ? '' : this.loadedList.city],
      country: [this.loadedList.country == '---' ? '' : this.loadedList.country],
      aboutThisPerson: [this.loadedList.aboutThisPerson == '---' ? '' : this.loadedList.aboutThisPerson],
      discussion: [this.loadedList.discussion == '---' ? '' : this.loadedList.discussion],
      entryDateTime: [''],
      photo: [''],
      latitude: [''],
      longitude: [''],
      mobileOrPhone: this.formBuilder.array([
        this.initMobileOrPhone()
      ]),
      record: ['']

    });

    const control = <FormArray>this.editedForm.controls['mobileOrPhone'];
    control.removeAt(0);

    var mobileNumberArray = this.loadedList.mobileOrPhone.split(',');

    for(var i =0;i<mobileNumberArray.length;i++){

      (<FormArray>this.editedForm.controls['mobileOrPhone']).push(
        new FormGroup({
          mobileNumber: new FormControl(mobileNumberArray[i].trim(),
            Validators.compose([Validators.pattern('01[7|6|5|1|9|8]{1}[0-9]{8}'), Validators.required]))
        })
      )
    }

    if(this.platform.is('android')){
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if(canRequest) {

          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)

            .then(() =>{
                this.geolocation.getCurrentPosition(options).then((resp) => {
                  this.editedForm.controls['latitude'].setValue(resp.coords.latitude);
                  this.editedForm.controls['longitude'].setValue(resp.coords.longitude);
                }).catch((error) => {
                  console.log('Error getting location', error);
                })

              },
              error => console.log('Error requesting location permissions', error)
            );
        }

      });
    }

    this.editedForm.controls['entryDateTime'].setValue(new Date().toLocaleString());
    console.log(this.editedForm.value);
  }

  initMobileOrPhone(){

    return this.formBuilder.group({
      mobileNumber:['',Validators.compose([Validators.pattern('01[7|6|5|1|9|8]{1}[0-9]{8}'), Validators.required])]
    });
  }


  addMobile(){
    const control = <FormArray>this.editedForm.controls['mobileOrPhone'];
    control.push(this.initMobileOrPhone());
  }

  submitValue(){

    this.l2nHttp.post('visitor-edit',this.editedForm.value, 'Saving data').then(
      data => {
        console.log('success');
        console.log(data);
        this.navCtrl.popToRoot();
      }, err => {
        console.log(err);
      }
    );

  }


  removeMobile(i: number) {
    const control = <FormArray>this.editedForm.controls['mobileOrPhone'];
    control.removeAt(i);
  }

  addMedia(){

    const actionSheet = this.addPhotoActionSheet.create({
      title: 'Add Images',
      cssClass:'custom-font',
      buttons: [
        {
          text: 'Take Photo',
          icon:'ios-camera-outline',
          handler: () => {
            this.openCamera();
          }
        },

        {
          text: 'Photo From Gallery',
          icon:'ios-albums-outline',
          handler: () => {
            this.selectFromGallery();
          }
        }
      ]
    });

    actionSheet.present();
  }

  selectFromGallery() {

    var options = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType:this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.editedForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });
  }

  openCamera() {

    var options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      targetWidth: 164,
      targetHeight:164,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.editedForm.controls['photo'].setValue(this.base64Image);
    }, (err) => {
      // Handle error
    });

  }

}
