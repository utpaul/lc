import { Component } from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";
import {AuthProvider} from "../../providers/auth";
import {BlockUi} from "../../providers/block-ui";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private imageUrl:any;
  private loadedList =[];
  private loadedData =[];
  adminFlag:boolean=false;
  pieChartType:string='pie';
  pieChartLabels:string[]=['Rose','Lily','Beena','Bithee'];
  pieChartData:number[]=[];

  pieChartColors: any[] = [{
    backgroundColor: ['#FF8BA4','#69C3FF','#FFDC85','#E0E3EB']
  }];


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private l2nHttp:L2nHttp,
              private auth:AuthProvider,
              public platform: Platform,
              private blockUi: BlockUi) {

  }

  ionViewWillEnter() {

    this.platform.ready().then(() => {
      let a = this.blockUi.show('Signing you in...');
      this.imageUrl = API_CONFIG.misBase;
      if (this.auth.currentUser.department == 'Top Management') {

        this.pieChartData = [];
        this.adminFlag = true;
        this.l2nHttp.getRequest('recent-status', 'getting data...').then(
          data => {

            this.loadedData = data;
            this.loadedList = this.loadedData['list'];

            this.loadedData['piaChart'].forEach(el => {

              if (el.id == 1) {

                this.pieChartData.push(parseInt(el.total));
              } else if (el.id == 2) {

                this.pieChartData.push(parseInt(el.total));
              } else if (el.id == 3) {

                this.pieChartData.push(parseInt(el.total));
              } else if (el.id == 4) {

                this.pieChartData.push(parseInt(el.total));
              }

            });

          },
          err => {
          }
        );

      } else {

        this.adminFlag = false;
        this.l2nHttp.getRequest('recent-status', 'getting data...').then(
          data => {
            this.loadedList = data;
            console.log(this.loadedList);
          },
          err => {
          }
        );

      }
    })
  }

  open(value:string){
   this.navCtrl.push('EachPersonListPage',{id: value})
  }

}
