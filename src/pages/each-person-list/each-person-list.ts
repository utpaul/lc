import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import {L2nHttp} from "../../providers/l2n-http";
import {API_CONFIG} from "../../constants/api";

@IonicPage()
@Component({
  selector: 'page-each-person-list',
  templateUrl: 'each-person-list.html',
})
export class EachPersonListPage {

  private imageUrl:any;
  private loadedList =[];
  itemExpendedHeight:number=200;
  private loadedData =[];
  private converter:any;
  private personName:string;

  constructor(public navParams: NavParams,
              private l2nHttp:L2nHttp) {
    this.imageUrl =API_CONFIG.misBase;
  }

  ionViewWillEnter(){

    if(parseInt(this.navParams.get('id')) == 1){
      this.personName ='Rose';
    } else if(parseInt(this.navParams.get('id')) == 2){
      this.personName ='Lily';
    }  else if(parseInt(this.navParams.get('id')) == 3){
      this.personName ='Beena';
    }else {
      this.personName ='Bithee';
    }

    this.l2nHttp.getRequest('each-user-list/'+this.navParams.get('id'),'getting data...')
      .then(
        data => {
          this.loadedData=data;

          this.loadedData.forEach((el,index)=>{
            this.loadedData[index].eachDayUser =this.loadedData[index].children.length;
            this.converter =(el.entrydate).split('-');
            if(parseInt(this.converter[1]) ==1){
              el.entrydate =this.converter[0]+' Jan '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==2){
              el.entrydate =this.converter[0]+' Feb '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==3){
              el.entrydate =this.converter[0]+' Mar '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==4){
              el.entrydate =this.converter[0]+' Apr '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==5){
              el.entrydate =this.converter[0]+' May '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==6){
              el.entrydate =this.converter[0]+' Jun '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==7){
              el.entrydate =this.converter[0]+' Jul '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==8){
              el.entrydate =this.converter[0]+' Aug '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==9){
              el.entrydate =this.converter[0]+' Sep '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==10){
              el.entrydate =this.converter[0]+' Oct '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==11){
              el.entrydate =this.converter[0]+' Nov '+this.converter[2];
            }else if(parseInt(this.converter[1]) ==12){
              el.entrydate =this.converter[0]+' Dec '+this.converter[2];
            }

          });

          this.loadedList = this.loadedData;

          console.log(this.loadedList);
        },
        err => {}
    );

  }

  toggleSection(i) {

    this.loadedList[i].open = !this.loadedList[i].open;
  }

  toggleItemSection(i,j){
    this.loadedList[i].children[j].open = !this.loadedList[i].children[j].open;
  }
}
