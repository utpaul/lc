import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EachPersonListPage } from './each-person-list';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    EachPersonListPage,
  ],
  imports: [
    IonicPageModule.forChild(EachPersonListPage),
    ComponentsModule
  ],
  exports: [
    EachPersonListPage
  ]
})
export class EachPersonListPageModule {}
