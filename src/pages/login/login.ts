import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {BlockUi} from "../../providers/block-ui";
import {NetworkConnectionProvider} from "../../providers/networkConnection";
import {AuthProvider} from "../../providers/auth";

@IonicPage({
  name: 'login-page',
  priority: 'high'
})

@Component({
  selector: 'login-page',
  templateUrl: 'login.html',
})
export class LoginPage {

  isOnline: boolean = false;
  profilePicture: any = "assets/logo.png";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              private blockUi: BlockUi,
              private networkCheck:NetworkConnectionProvider,
              private auth: AuthProvider) {

    this.platform.ready().then(() => {
      this.isOnline =this.networkCheck.isOnline;
    });

  }

  emailChanged(){
    this.profilePicture = "https://www.gravatar.com/avatar/";
  }

  login(FormLogin){

    this.platform.ready().then(() => {
      let a =this.blockUi.show('Signing you in...');

      this.auth.login( FormLogin.value.email,FormLogin.value.password)
        .then(data =>{
          a.dismiss();
        }).catch(error =>{
        a.dismiss();
        this.blockUi.alert('Sorry! Invalid Username or Password', 'Sign In Problem!');
      })

    })
  }

}
