import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {L2nHttp} from "../../providers/l2n-http";
import {API_CONFIG} from "../../constants/api";

@IonicPage()
@Component({
  selector: 'page-visitors',
  templateUrl: 'visitors.html',
})
export class VisitorsPage {

  registrationForm = 'RegistrationPage';
  private loadedList = [];
  private imageUrl:any;

  reVisited = 'ReVisitMemberPage';

  constructor(public navCtrl: NavController,
              private l2nHttp:L2nHttp) {
    this.imageUrl =API_CONFIG.misBase;
  }

  searchMemberList(FormSubmit){

    console.log(FormSubmit.value);
    this.loadedList =[];
    this.l2nHttp.post('visitor-search',FormSubmit.value, 'Getting data').then(
      data => {
        this.loadedList = data;
      }, err => {
        console.log(err);
      }
    );

  }

  ionViewDidEnter(){
    console.log('call enter method');
  }

}
