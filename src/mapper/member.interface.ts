export interface Member{
  id:string,
  fullName:string,
  nickName:string,
  mobileOrPhone:string,
  email:string,
  address:string,
  area:string,
  city:string,
  country: string,
  photo:string,
  visited:string
}
