export const APP_MENUS = {
    sidebar: [
      {
        label: 'Visitors',
        cache: false,
        icon: 'ios-contacts',
        component: 'VisitorsPage'
      },
      {
        label: 'List of Visitors',
        cache: false,
        icon: 'ios-list-box',
        component: 'ListOfVisitorsPage'
      }
    ]
};
