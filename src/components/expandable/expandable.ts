import {Component, Input} from '@angular/core';

@Component({
  selector: 'expandable',
  templateUrl: 'expandable.html'
})
export class ExpandableComponent {

  @Input('expended') expended;
  @Input('expendHeight') expendHeight;

  constructor() {
    console.log(this.expended);
  }

  ngAfterViewInit(){
    console.log(this.expended);
    console.log(this.expendHeight);
  }
}
