import { NgModule } from '@angular/core';
import {Elastic} from "./elastic/elastic";
import {IonicModule} from "ionic-angular";
import {AudioInputComponent} from "./audio-input/audio-input";
import {ExpandableComponent} from "./expandable/expandable";

@NgModule({
  declarations: [
    Elastic,
    AudioInputComponent,
    ExpandableComponent
  ],
  imports: [
    IonicModule
  ],
  exports: [
    Elastic,
    AudioInputComponent,
    ExpandableComponent
  ]
})
export class ComponentsModule {

}
