import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera} from '@ionic-native/camera';
import { MyApp } from './app.component';
import {BlockUi} from "../providers/block-ui";
import {AuthProvider} from "../providers/auth";
import {Network} from "@ionic-native/network";
import {NetworkConnectionProvider} from "../providers/networkConnection";
import {L2nHttp} from "../providers/l2n-http";
import {HttpModule} from "@angular/http";
import {ImagePicker} from "@ionic-native/image-picker";
import { Geolocation } from '@ionic-native/geolocation';
import {MediaPlugin} from "@ionic-native/media";
import {AudioRecordingModal} from "../components/audio-input/audio-recording";
import {File} from '@ionic-native/file';
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {HomePage} from "../pages/home/home";
import {ChartsModule} from "ng2-charts";

declare var require: any;

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AudioRecordingModal
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ChartsModule,
    IonicModule.forRoot(MyApp)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AudioRecordingModal
  ],
  providers: [
    Geolocation,
    StatusBar,
    SplashScreen,
    AuthProvider,
    BlockUi,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Network,
    NetworkConnectionProvider,
    L2nHttp,
    Camera,
    ImagePicker,
    MediaPlugin,
    File,
    LocationAccuracy
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
