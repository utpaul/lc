import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {User} from "../mapper/user";
import {NetworkConnectionProvider} from "../providers/networkConnection";
import {AuthProvider} from "../providers/auth";
import {APP_MENUS} from "../constants/menu";
import {HomePage} from "../pages/home/home";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  isAuthenticated: boolean = false;
  user: User;
  rootPage: any;

  pages: Array<{title: string, icon: string, component: any}> = [];

  constructor(public platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private auth:AuthProvider,
              private networkCheck:NetworkConnectionProvider,
              private eventCtrl: Events) {

    this.initializeApp(statusBar, splashScreen);

  }

  initializeApp(statusBar: StatusBar,
                splashScreen: SplashScreen) {
    this.platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      this.auth.onSessionStore.subscribe(() => {
        if (this.auth.isAuthenticated == true) {
          this.isAuthenticated = true;
          this.user = new User(this.auth.currentUser);

          this.nav.setRoot(HomePage).then(() => {

            APP_MENUS.sidebar.forEach(el => {

              if(this.user.department =='Top Management' && el.label == 'Visitors'){

              }else{
                this.pages.push({title: el.label, icon: el.icon, component: el.component});
              }
            });

          });

        } else {
          this.nav.setRoot('login-page').then(() => {
            this.isAuthenticated = false;
          });
        }
      });

      this.eventCtrl.subscribe('network:online', () => {

      });

      this.eventCtrl.subscribe('network:offline', () => {

      });
    });
  }

  logout() {

    this.nav.setRoot('login-page').then(() => {
      this.auth.destroyUserCredentials();
      this.pages=[];
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }

}

